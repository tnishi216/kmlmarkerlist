package com.reliefoffice.kmlmarkerlist;

import android.Manifest;
import android.app.Service;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Binder;
import android.os.Build;
import android.os.IBinder;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;

import java.io.File;

public class GpsLoggerService extends Service implements LocationListener {
    private final IBinder binder = new LocalBinder();
    public class LocalBinder extends Binder {
        GpsLoggerService getService() {
            return GpsLoggerService.this;
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_STICKY; // システムによって終了された場合に再起動する
    }

    @Override
    public void onDestroy() {
        flushGpsLogger();
        stopLog();
        stopGps();
        super.onDestroy();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }
    // ------------------------------------------------------------------------------------
    // GPS Receiver
    // ------------------------------------------------------------------------------------
    LocationManager mLocationManager;
    GpsStatus.Listener mGpsStatusListener;
    String bestProvider;
    int locationInterval = config.LocationMinTime;  // 更新間隔[sec]

    public boolean isGpsStarted(){
        return mLocationManager != null;
    }
    public boolean startGps(Criteria criteria){
        if (mLocationManager != null) return true;  // already started
        mLocationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        bestProvider = mLocationManager.getBestProvider(criteria, true);

        mGpsStatusListener = new GpsStatus.Listener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            public void onGpsStatusChanged(int event) {
                //TODO: なぜかこのif文を追加しないとmLocationManager.getGpsStatue(null)でエラーになる？？？
                if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    Activity#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for Activity#requestPermissions for more details.
                    return;
                }
            }
        };
        mLocationManager.addGpsStatusListener(mGpsStatusListener);  // ここから権限が必要らしい

        startLocation();

        return true;
    }
    public void stopGps(){
        if (mLocationManager == null) return;
        stopLocation();
        mLocationManager = null;
    }
    public void startLocation() {
        if (mLocationManager == null) return;
        mLocationManager.requestLocationUpdates(bestProvider, locationInterval*1000, config.LocationMinDistance, this);
    }

    public void stopLocation() {
        if (mLocationManager == null) return;
        mLocationManager.removeUpdates(this);
    }

    @Override
    public void onLocationChanged(@NonNull Location location) {
        updateGpsLogger(location);
    }

    // ------------------------------------------------------------------------------------
    // GPS Logger
    // ------------------------------------------------------------------------------------
    GPXWriter gpxWriter;
    int gpsPoint;
    long gpsStartTime;

    public boolean startLog(String filename, long starttime){
        if (gpxWriter != null) {
            return true;    // already started
        }

        gpxWriter = new GPXWriter();
        File file = new File(filename);
        if (!gpxWriter.open(file)){
            gpxWriter = null;
            return false;
        }
        gpxWriter.flush();

        if (starttime == 0)
            gpsStartTime = System.currentTimeMillis();
        else
            gpsStartTime = starttime;
        gpsPoint = 0;   //TODO: ファイルから読み込む

        return true;
    }
    public void stopLog(){
        if (gpxWriter == null){
            return;
        }
        gpxWriter.close();
        gpxWriter = null;
    }
    public boolean isLogStarted(){
        return gpxWriter != null;
    }
    public int getNumPoints(){ return gpsPoint; }
    public int getPastTime() { return (int)((System.currentTimeMillis() - gpsStartTime)/1000); }
    void updateGpsLogger(Location location)
    {
        if (gpxWriter == null) return;
        gpxWriter.append(location);
//        gpxWriter.flush();    // 例外が発生しなければこれがなくても問題は無いが。。
        gpsPoint++;
    }
    void flushGpsLogger()
    {
        if (gpxWriter != null)
            gpxWriter.flush();
    }
}
