package com.reliefoffice.kmlmarkerlist;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

public class GPXReader {
    public static class GpxData {
        public double latitude;
        public double longitude;
        public Date time;

        public GpxData(double latitude, double longitude, Date time) {
            this.latitude = latitude;
            this.longitude = longitude;
            this.time = time;
        }
    }
    public List<GpxData> readAll(File gpxFile) {
        List<GpxData> gpxList = new ArrayList<>();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        dateFormat.setTimeZone(java.util.TimeZone.getTimeZone("UTC"));

        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(gpxFile);
            doc.getDocumentElement().normalize();

            NodeList nList = doc.getElementsByTagName("trkpt");

            for (int i = 0; i < nList.getLength(); i++) {
                Node nNode = nList.item(i);
                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) nNode;

                    // 緯度と経度を取得
                    double lat = Double.parseDouble(eElement.getAttribute("lat"));
                    double lon = Double.parseDouble(eElement.getAttribute("lon"));

                    // 時刻情報を取得
                    Date time = null;
                    NodeList timeList = eElement.getElementsByTagName("time");
                    if (timeList.getLength() > 0) {
                        String timeStr = timeList.item(0).getTextContent();
                        try {
                            time = dateFormat.parse(timeStr);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    }

                    // リストに追加
                    gpxList.add(new GpxData(lat, lon, time));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return gpxList;
    }
}
