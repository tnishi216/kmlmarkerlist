package com.reliefoffice.kmlmarkerlist;

public class config {
    public static final String PrefName = "com.reliefoffice.kmlmarker_preferences";
    public static final int MaxFileHistoryNum = 100;
    public static final int LocationMinTime = 3;    // GPS更新間隔[sec]
    public static final float LocationMinDistance = 1;  // GPS更新最低距離[m]
    public static final int MaxStrokeInterval = 3000;   // [msec] Mark buttonの連打と判断する最大の間隔（＝mark保存までのdelay time）
    public static final String DefaultLastFileName = "/storage/emulated/0/Download/POINT.kml";
    public static final String UPLOAD_URL1 = "http://192.168.1.2/cgi-bin/upload/upload.cgi";
    public static final String UPLOAD_URL2 = "https://pdic.sakura.ne.jp/cgi-bin/upload/upload.cgi";
    public static final int MAX_THREADS_FOR_FILE_UPLOAD = 4;
    public static final int MAX_MARK_NAME_HISTORY = 10; // カスタムボタンの名前の最大履歴数

    public static final int COLOR_RED = 0xFF0000;
    public static final int COLOR_GREEN = 0x00FF00;
    public static final int COLOR_BLUE = 0x0000FF;
    public static final int COLOR_YELLOW = 0xFFFF00;
    public static final int COLOR_MAGENTA = 0xFF00FF;
    public static final int COLOR_ORANGE = 0xFFA500;
    public static final int COLOR_CYAN = 0x00FFFF;
    public static final int COLOR_PINK = 0xFFC0CB;
    public static final int COLOR_BLACK = 0x000000;
    public static final int COLOR_INVALID = 0x7F000000;

    public final static String DEBUG_GPS_EMULATION_FILE = "/storage/emulated/0/Download/debug.gpx";
}
