package com.reliefoffice.kmlmarkerlist;

import android.location.Location;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;

import java.io.File;
import java.util.List;

public class GPSEmulator {
    private OnGpsEmulatorListener listener;
    private boolean isEmulating = false;

    public interface OnGpsEmulatorListener {
        void onLocationChanged(Location location);
        void onEmulationFinished();
    }

    public void setGpsEmulatorListener(OnGpsEmulatorListener listener) {
        this.listener = listener;
    }
    List<GPXReader.GpxData> items;
    boolean stopFlag;

    public boolean startEmulation(String filename) {
        if (isEmulating) return false;

        GPXReader reader = new GPXReader();
        List<GPXReader.GpxData> items = reader.readAll(new File(filename));
        if (items.size() == 0){
            return false;
        }

        isEmulating = true;

        new Thread(new Runnable() {
            @Override
            public void run() {
                Location prevLocation = null;
                for (int i=0;i<items.size();i++) {
                    if (stopFlag) break;
                    GPXReader.GpxData data = items.get(i);
                    Location location = new Location("");
                    location.setLongitude(data.longitude);
                    location.setLatitude(data.latitude);
                    if (prevLocation != null){
                        if (prevLocation.getLongitude() == location.getLongitude() && prevLocation.getLatitude() == location.getLatitude()){
//                            continue;
                        }
                    }
                    prevLocation = location;
                    if (location != null && listener != null) {
                        callLocationChanged(location);
                    }
                    SystemClock.sleep(1000); // Emulate real-time movement
                }
                isEmulating = false;
                if (listener != null) {
                    callEmulationFinished();
                }
            }
        }).start();

        return true;
    }
    public void stopEmulation(){
        if (!isEmulating) return;
        stopFlag = true;
    }
    void callLocationChanged(Location location)
    {
        Handler mainThreadHandler = new Handler(Looper.getMainLooper());
        mainThreadHandler.post(new Runnable() {
            @Override
            public void run() {
                // in main thread
                if (listener != null) {
                    listener.onLocationChanged(location);
                }
            }
        });
    }
    void callEmulationFinished(){
        Handler mainThreadHandler = new Handler(Looper.getMainLooper());
        mainThreadHandler.post(new Runnable() {
            @Override
            public void run() {
                // in main thread
                if (listener != null) {
                    listener.onEmulationFinished();
                }
            }
        });
    }
}
