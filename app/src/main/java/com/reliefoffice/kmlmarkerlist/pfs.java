package com.reliefoffice.kmlmarkerlist;

public class pfs {
    public static final String LASTLAT = "LastLat";
    public static final String LASTLONG = "LastLong";
    public static final String LASTFILENAME = "LastFileName";
    public static final String PSINITIALDIR = "PSInitialDir";
    public static final String INITIALDIR = "InitialDir";
    public static final String LOCATIONINTERVAL = "LocationInterval";
    public static final String GPSLOGGERENABLED = "GpsLoggerEnabled";
    public static final String GPSLOGFILENAME = "GpsLogFilename";
    public static final String GPSLOGSTARTTIME = "GpsLogStartTime"; // start time [msec]
    public static final String GPSLOGNUMPOINTS = "GpsLogNumPoints";
    public static final String DEFAULTDEFMARKNAME = "DefaultMarkName";
    public static final String MARK_NAME_HISTORY = "MarkNameHistory";
    public static final String MARK_NAME_HISTORY_COLORS = "MarkNameHistoryColors";
    public static final String LAST_MARK_COLOR = "LastMarkColor";
}
