package com.reliefoffice.kmlmarkerlist;

import android.content.Context;
import android.graphics.Color;

import com.reliefoffice.kmlmarkerlist.R;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class MarkItem {
    public String name;
    public String comment;
    public int color;

    // 以下の２つは個別に持たず、都度計算したいのだが、sortがあるので難しいところ
    public double distance;
    public int direction;

    public double longitude;
    public double latitude;
    public int altitude;
    private Date date = null;   // UTC
    MarkItem(){
        color = config.COLOR_INVALID;
        distance = 0;
        direction = 0;
        longitude = 0;
        latitude = 0;
        altitude = 0;
    }
    MarkItem(String name, String comment, double longitude, double latitude, int altitude){
        this.name = name;
        this.comment = comment;
        color = config.COLOR_INVALID;
        distance = 0;
        direction = 0;
        this.longitude = longitude;
        this.latitude = latitude;
        this.altitude = altitude;
    }
    MarkItem(String name, String comment, int color, int distance, int direction){
        this.name = name;
        this.comment = comment;
        this.color = color;
        this.distance = distance;
        this.direction = direction;
    }

    public boolean isDateNull() { return date == null; }
    // dateはUTC
    public void setUTCDate(Date date)
    {
        this.date = date;
    }
    // dateはlocal time
    public void setLocalDate(Date date)
    {
        this.date = Utility.dateToUTC(date);
    }
    final public int getColor()
    {
        //TODO: もっと簡単にならない？
        return Color.rgb(this.color>>16, (this.color>>8)&0xFF, this.color&0xFF);
    }
    public String getDateDispString()
    {
        if (date == null)
            return null;

        return new SimpleDateFormat("HH:mm", Locale.getDefault()).format(Utility.dateToLocal(this.date));
    }
    public String getDateString()
    {
        if (date == null)
            return null;
        return new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.getDefault()).format(date);
    }
    // 実際の表示テキストはMarkItemLayout.bindView()
    @Override
    public String toString(){
        String str = name;
        if (Utility.isNotEmpty(comment))
            str += " " + comment;
        if (distance != 0)
            str += "(" + String.valueOf(distance) + ")";
        return str;
    }
    public static String mapName(Context context, String name){
        if (name.equals("MarkA")){
            return context.getString(R.string.label_mark_a);
        } else
        if (name.equals("MarkB")){
            return context.getString(R.string.label_mark_b);
        } else
        if (name.equals("MarkC")){
            return context.getString(R.string.label_mark_c);
        } else
        if (name.equals("MarkD")){
            return context.getString(R.string.label_mark_d);
        } else {
            return name;
        }
    }
}
