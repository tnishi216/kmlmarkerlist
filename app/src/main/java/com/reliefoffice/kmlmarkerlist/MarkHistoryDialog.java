package com.reliefoffice.kmlmarkerlist;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.PopupMenu;

import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.reliefoffice.kmlmarkerlist.R;

import java.util.List;

public class MarkHistoryDialog extends DialogFragment implements Utility.DialogClickListener, MarkEditDialog.MarkEditDialogListener {
    private static final String ARG_PARAM1 = "param1";
    private String mParam1;
    public static MarkHistoryDialog newInstance(Context context, String markfilename) {
        KMLReader reader = new KMLReader(context);
        List<MarkItem> items = reader.load(markfilename);
        if (items.size() == 0){
            Utility.showMessageDialog(context, context.getString(R.string.msg_no_history),null);
            return null;
        }
        MarkHistoryDialog fragment = new MarkHistoryDialog(context, items, markfilename);
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, markfilename);
//        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    Context context;
    private List<MarkItem> items;
    String markFilename;
    private MarkHistoryAdapter markHistoryAdapter;
    ListView markHistory;
    MarkHistoryDialog(Context context, List<MarkItem> items, String markfilename){
        this.context = context;
        this.items = items;
        this.markFilename = markfilename;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
//            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater inflater = requireActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.fragment_mark_history, null);

        markHistoryAdapter = new MarkHistoryAdapter(getContext(), R.layout.list_item_markhistory);
        markHistory = (ListView) view.findViewById(R.id.markHistory);
        markHistory.setAdapter(markHistoryAdapter);
        markHistoryAdapter.setList(items);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(view)
                .setPositiveButton(getString(R.string.label_close), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
//                        String name = editName.getText().toString();
//                        String comment = editComment.getText().toString();
//                        listener.onMarkEditOnClick(name, comment);
                    }
                });
//                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
//                    @Overridend0
//                    public void onClick(DialogInterface dialog, int which) {
////                        listener.onMarkEditCancelClick();
//                    }
//                });


        // Listeners
        markHistory.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                startGoogleMap(position);
            }
        });
        markHistory.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int position, long l) {
                openMenu(view, position);
                return true;
            }
        });

        return builder.create();
    }

    @Override
    public void onResume() {
        super.onResume();
        Utility.requestFocus(markHistory, getContext());
    }

    void startGoogleMap(int position){
        MarkItem item = markHistoryAdapter.getItem(position);
        Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse("geo:0,0?q="+item.latitude+","+item.longitude+"("+item.name+")"));
        startActivity(i);
    }
    int tmpPosition;
    private void openMenu(View view, int position) {
        PopupMenu popupMenu = new PopupMenu(requireContext(), view);
        popupMenu.getMenuInflater().inflate(R.menu.menu_mark_history, popupMenu.getMenu());

        // メニュー項目のクリックリスナーを設定
        popupMenu.setOnMenuItemClickListener(item -> {
            switch (item.getItemId()) {
                case R.id.action_edit:
                    edit(position);
                    return true;
                case R.id.action_delete:
                    tmpPosition = position;
                    Utility.confirmDialog(requireContext(), "削除してもいいですか？", this);
                    return true;
                default:
                    return false;
            }
        });

        popupMenu.show();
    }

    void edit(int position)
    {
        MarkItem item = markHistoryAdapter.getItem(position);
        tmpPosition = position;
        MarkEditDialog dialog = MarkEditDialog.newInstance(this, item);
        dialog.show(requireFragmentManager(), "mark_edit_dialog");
    }
    @Override
    public void onMarkEditOnClick(String name, String comment, int color, @Nullable MarkItem item) {
        MarkItem orgItem = markHistoryAdapter.getItem(tmpPosition);
        orgItem.name = name;
        orgItem.comment = comment;
        orgItem.color = color;
        markHistoryAdapter.notifyDataSetChanged();
    }

    @Override
    public void onMarkEditCancelClick() {
    }
    @Override
    public void onYesClick() {
        MarkItem item = items.get(tmpPosition);
        markHistoryAdapter.remove(item);
        KMLEditor.remove(requireContext(), markFilename, tmpPosition);
    }
}
