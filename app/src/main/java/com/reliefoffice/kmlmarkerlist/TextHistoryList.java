package com.reliefoffice.kmlmarkerlist;

import android.content.SharedPreferences;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class TextHistoryList {
    private int maxHistory;
    private List<String> historyList;   // indexが小さい方が新しい
    private Map<String, Integer> colors;  // Text -> Color mapping

    public TextHistoryList(int maxHistory) {
        this.maxHistory = maxHistory;
        historyList = new LinkedList<>();
        colors = new HashMap<>();
    }

    public int size() {
        return historyList.size();
    }

    // return:
    //  true: 更新あり
    //  false: 変更なし
    public boolean add(String text, int color) {
        if (historyList.size() > 0){
            if (historyList.get(0) == text)
                return false;   // 何もしないで返す
        }
        // Remove duplicates if already exists
        if (historyList.contains(text)) {
            historyList.remove(text);
        }
        // Add the new input text to the top (most recent)
        historyList.add(0, text);
        colors.put(text, color);
        // Keep only the last MAX_HISTORY texts
        if (historyList.size() > maxHistory) {
            historyList.remove(maxHistory);
            colors.remove(text);
        }
        return true;
    }

    public void load(SharedPreferences pref, String keynameHistory, String keynameColors)
    {
        historyList = Utility.loadList(keynameHistory, pref);
        colors = Utility.loadStringIntMap(keynameColors, pref);
    }
    public void save(SharedPreferences pref, String keynameHistory, String keynameColors)
    {
        SharedPreferences.Editor editor = pref.edit();
        Utility.saveList(keynameHistory, historyList, editor);
        Utility.saveStringIntMap(keynameColors, colors, editor);
        editor.apply();
    }

    public List<String> getList() {
        return historyList;
    }
    public boolean hasText(String text){
        return historyList.contains(text);
    }
    public int getColor(String text) {
        if (colors.containsKey(text))
            return colors.get(text);
        return config.COLOR_INVALID;
    }
}
