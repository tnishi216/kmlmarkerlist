package com.reliefoffice.kmlmarkerlist;

import android.util.Log;

import java.util.Map;

public class MultiFileUploader {
    private final int MAX_THREADS = config.MAX_THREADS_FOR_FILE_UPLOAD;
    private FileUploader uploaders[] = new FileUploader[MAX_THREADS];
    private FileUploader.UploadProgressListener listener;
    private FileUploader.UploadProgressListener userListener;
    private int totalRequestCount = 0;
    private int totalCompleteCount = 0;
    private int totalFailedCount = 0;
    private boolean failed = false;
    private boolean stopped = false;
    private long startTime;

    public MultiFileUploader(String url, Map<String, String> params, FileUploader.UploadProgressListener listener)
    {
        userListener = listener;
        this.listener = new FileUploader.UploadProgressListener() {
            @Override
            public void onProgressUpdate(int queing, int uploaded, String uploadedImagePath) {
                int t_queing = 0;
                int t_uploaded = 0;
                for (int i=0;i<MAX_THREADS;i++){
                    t_queing += uploaders[i].getQueueingCount();
                    t_uploaded += uploaders[i].getUploadedCount();
                }
                userListener.onProgressUpdate(t_queing, t_uploaded, uploadedImagePath);
            }

            @Override
            public void onCompleted() {
                totalCompleteCount++;
                {
                    userListener.onCompleted();
                    int elap = (int)(System.currentTimeMillis() - startTime);
                    Log.i("MultiFileUp", "Elapsed time: "+Integer.toString(elap) + "[msec]");
                }
            }

            @Override
            public void onFailed(int responseCode) {
                totalFailedCount++;
                if (!failed) {
                    userListener.onFailed(responseCode);
                    failed = true;
                }
            }
        };
        for (int i=0;i<MAX_THREADS;i++){
            uploaders[i] = new FileUploader(url, params, this.listener);
        }
    }

    public void startUpload(String imagePath)
    {
        if (startTime == 0){
            startTime = System.currentTimeMillis();
        }
        int index = getMinQueIndex();
        uploaders[index].startUpload(imagePath);
        totalRequestCount++;
    }
    public int getMinQueIndex()
    {
        int min_que = 10000;
        int min_que_inx = 0;
        for (int i=0;i<MAX_THREADS;i++){
            int cnt = uploaders[i].getQueueingCount();
            if (cnt < min_que){
                min_que = cnt;
                min_que_inx = i;
            }
        }
        return min_que_inx;
    }

    public boolean isStop()
    {
        return stopped;
    }
    public void forceToStop()
    {
        stopped = true;
        for (int i=0;i<MAX_THREADS;i++){
            uploaders[i].forceToStop();
        }
    }
}
