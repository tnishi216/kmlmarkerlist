package com.reliefoffice.kmlmarkerlist;

import android.Manifest;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.GpsSatellite;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity implements LocationListener, MarkEditDialog.MarkEditDialogListener, GPSEmulator.OnGpsEmulatorListener {
    SharedPreferences pref;

    private MarkListAdapter markListAdapter;
    private LocationInfo locInfo = new LocationInfo();

    private Button btnMarkA;
    private Button btnMarkB;
    private Button btnMarkC;
    private Button btnMarkD;

    private TextView locationText;
    private TextView statusText;
    private TextView loggingText;

    private List<String> lastFileNames;
    private String m_strInitialDir;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        pref = getSharedPreferences(config.PrefName, Context.MODE_PRIVATE);

        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        markListAdapter = new MarkListAdapter(this, R.layout.list_item_marklist, locInfo);
        ListView markList = (ListView) findViewById(R.id.markList);
        markList.setAdapter(markListAdapter);

        // Mark Buttons
        btnMarkA = findViewById(R.id.btn_mark_a);
        btnMarkB = findViewById(R.id.btn_mark_b);
        btnMarkC = findViewById(R.id.btn_mark_c);
        btnMarkD = findViewById(R.id.btn_mark_d);
        btnMarkA.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                btnMarkAClick();
            }
        });
        btnMarkB.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                btnMarkBClick();
            }
        });
        btnMarkC.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                btnMarkCClick();
            }
        });
        btnMarkD.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                btnMarkDClick();
            }
        });

        locationText = (TextView) findViewById(R.id.locationText);
        statusText = (TextView) findViewById(R.id.statusText);
        loggingText = (TextView) findViewById(R.id.loggingText);

        // Listeners
        markList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                startGoogleMap(position);
            }
        });
        markList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int position, long l) {
                startGoogleMapNavi(position);
                return true;
            }
        });

        locationText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startGoogleMapHere();
            }
        });
        loggingText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) { showLoggingStatus(); }
        });

        initLocationManager();

        if (use_service){
            startGpsLoggerService();
        }
        initGpsLogger();

        openMarks();
    }

    @Override
    protected void onStart() {
        super.onStart();

        locationStart();

        if (!Utility.requestStorageReadPermission(this)) {
            return;
        }

        locInfo.load(pref);
        updateLocation();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onStop() {
        super.onStop();

        flushPendingMarkItem();
        flushGpsLogger();

        locationStop();

        // Save location
        locInfo.save(pref);
    }

    @Override
    protected void onDestroy() {
        if (use_service) {
            if (serviceConnection != null){
                boolean started = isGpsLogStarted();
                unbindService(serviceConnection);
                serviceConnection = null;
                if (!started) {
                    // GPS loggingが停止時はServiceを完全停止
                    stopGpsLoggerService();
                }
            }
        }
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.action_gps_emulate);
        // fragmentの切替え時、このitemがnullになるときがあるため（原因不明）
        if (item != null) {
            // 動的に切り替わるメニュー
            item.setVisible(existsGpsEmulatorFile());
        }
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_open){
            mergeMode = false;
            openFile();
        } else
        if (id == R.id.action_reopen){
            openFileFromHistory();
        } else
        if (id == R.id.action_import){
            importFile();
        } else
        if (id == R.id.action_gps_logger_toggle){
            toggleGpsLogger();
        } else
        if (id == R.id.action_show_mark_history){
            showMarkHistory();
        } else
        if (id == R.id.action_upload){
            actionUpload();
        } else
        if (id == R.id.action_select_location_interval) {
            MySelectLocationIntervalDialog dialog = new MySelectLocationIntervalDialog();
            dialog.main = this;
            dialog.setSelect(locationInterval);
            dialog.show(getFragmentManager(), "location_interval");
        } else
        if (id == R.id.action_settings) {
            SettingDialog dialog = SettingDialog.newInstance(this);
            dialog.show(getSupportFragmentManager(), "setting");
            return true;
        } else
        if (id == R.id.action_gps_emulate) {
            emulateGps();
        }

        return super.onOptionsItemSelected(item);
    }

    // --------------------------------------- //
    // GPS Logger Service
    // --------------------------------------- //
    boolean use_service = true;
    static Intent gpsLoggerServiceIntent; //TODO: MainActivityが破棄されたあとも再び再利用する場合があるためstaticにしているが、本当にこれでいいの？？
    GpsLoggerService gpsLoggerService;
    ServiceConnection serviceConnection;
    boolean startGpsLoggerService()
    {
        if (!use_service) return false;
        if (gpsLoggerServiceIntent == null){
            Intent intent = new Intent(this, GpsLoggerService.class);
            startService(intent);
            gpsLoggerServiceIntent = intent;
        }
        if (serviceConnection == null){
            serviceConnection = new ServiceConnection() {
                @Override
                public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
                    GpsLoggerService.LocalBinder binder = (GpsLoggerService.LocalBinder) iBinder;
                    gpsLoggerService = binder.getService();
                    Log.i("PDD", "GpsLoggingService connected.");
                }

                @Override
                public void onServiceDisconnected(ComponentName componentName) {
                    gpsLoggerService = null;
                }
            };
            bindService(gpsLoggerServiceIntent, serviceConnection, Context.BIND_AUTO_CREATE);
        }
        return true;
    }
    void stopGpsLoggerService()
    {
        if (!use_service) return;
        if (gpsLoggerServiceIntent != null) {
            stopService(gpsLoggerServiceIntent);
            gpsLoggerServiceIntent = null;
            gpsLoggerService = null;
        }
    }
    boolean isGpsLoggerServiceStarted()
    {
        return gpsLoggerService != null;
    }
    boolean isGpsLogStarted(){
        if (gpsLoggerService == null) return false;
        return gpsLoggerService.isLogStarted();
    }


    public static class MySelectLocationIntervalDialog extends SelectLocationIntervalDialog {
        public MainActivity main;
        @Override
        protected void onSelectLocationIntervalSet(int value){
            main.onSelectLocationIntervalSet(value);
        }
    }
    void onSelectLocationIntervalSet(int value){
        if (locationInterval != value) {
            locationInterval = value;
            SharedPreferences.Editor edit = pref.edit();
            edit.putInt(pfs.LOCATIONINTERVAL, locationInterval);
            edit.apply();
            locationStop();
            locationStart();
        }
        String msg = getString(R.string.msg_location_interval_changed) + value + getString(R.string.label_second);
        Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == Utility.REQUEST_CODE_PERMISSION) {
            if (!Utility.permissionGranted(grantResults)) {
                finish();
            }
            return;
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    static final int REQUEST_CODE_SELECT_FILE = 1;
    static final int REQUEST_CODE_SELECT_HISTORY = 2;

    String fileEncoding;
    boolean mergeMode = false;

    private void openFile(){
        Intent i = new Intent().setClassName(this.getPackageName(), FileDirSelectionActivity.class.getName());
        i.putExtra(pfs.INITIALDIR, pref.getString(pfs.PSINITIALDIR, Utility.initialFileDirectory()));
        String[] exts = {".kml"};
        i.putExtra("exts",exts);
        startActivityForResult(i, REQUEST_CODE_SELECT_FILE);
    }

    private void openFileFromHistory() {
        mergeMode = false;
        Intent i = new Intent().setClassName(this.getPackageName(), FileHistorySelectionActivity.class.getName());
        startActivityForResult(i, REQUEST_CODE_SELECT_HISTORY);
    }

    private void importFile(){
        mergeMode = true;
        openFile();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_SELECT_FILE) {
            if (resultCode == RESULT_OK) {
                Bundle bundle = data.getExtras();
                if (bundle != null) {
                    String filename = bundle.getString("filename");
                    if (Utility.isNotEmpty(filename)) {
                        File file = new File(filename);
                        String name = file.getName();
                        fileEncoding = bundle.getString("fileEncoding");
                        FileInfo fileInfo = new FileInfo(name, file);
                        onFileSelect(fileInfo);
                    }
                }
            }
        } else
        if (requestCode == REQUEST_CODE_SELECT_HISTORY) {
            if (resultCode == RESULT_OK) {
                Bundle bundle = data.getExtras();
                if (bundle != null) {
                    String filename = bundle.getString("filename");
                    // String remotename = bundle.getString("remotename");
                    if (Utility.isEmpty(filename)) {
                    } else {
                        // File Load from History
                        fileEncoding = bundle.getString("fileEncoding");
                        FileInfo file = new FileInfo(filename);
                        onFileSelect(file);
                    }
                }
            }
        }
    }

    public void onFileSelect(FileInfo file) {
        if (!mergeMode){
            lastFileNames.clear();
        }
        lastFileNames.add(file.getPath());

        m_strInitialDir = file.getParent();
        SharedPreferences.Editor edit = pref.edit();
        edit.putString(pfs.PSINITIALDIR, m_strInitialDir);
        Utility.saveList(pfs.LASTFILENAME, lastFileNames, edit);
        edit.apply();

        FileHistoryManager mgr = new FileHistoryManager(this);
        mgr.add(file.getPath(), fileEncoding);

        openMarks();
    }

    void openMarks(){
        lastFileNames = Utility.loadList(pfs.LASTFILENAME, pref);
        if (lastFileNames.size() == 0){
            lastFileNames.add(config.DefaultLastFileName);
        }

        markListAdapter.clear();
        for (int i=0;i<lastFileNames.size();i++){
            List<MarkItem> list = loadKmlFile(lastFileNames.get(i));
            markListAdapter.addList(list);
        }

        markListAdapter.notifyDataSetChanged();
    }

    public List<MarkItem> loadKmlFile(String filename)
    {
        KMLReader reader = new KMLReader(this);
        return reader.load(filename);
    }
    void saveKmlFile(String filename, List<MarkItem> items)
    {
        KMLWriter writer = new KMLWriter();
        File file = new File(filename);
        if (!writer.open(file)){
            Toast.makeText(this, "Failed to open file: "+filename, Toast.LENGTH_LONG).show();
            return;
        }
        writer.write(items);
        writer.close();
    }

    LocationManager mLocationManager;
    String bestProvider;

    GpsStatus.Listener mGpsStatusListener;

    int locationInterval = config.LocationMinTime;  // 更新間隔[sec]

    int lastGpsStatus = GpsStatus.GPS_EVENT_STOPPED;
    int numGps = 0;
    int numUseGps = 0;  // should be numUseGps <= numGps

    private void initLocationManager() {
        locationInterval = pref.getInt(pfs.LOCATIONINTERVAL, locationInterval);

        // インスタンス生成
        mLocationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

        // 詳細設定
        Criteria criteria = getCriteria();
        bestProvider = mLocationManager.getBestProvider(criteria, true);
        Log.i("KML", "Best Provider is "+bestProvider);

        mGpsStatusListener = new GpsStatus.Listener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            public void onGpsStatusChanged(int event) {
                //TODO: なぜかこのif文を追加しないとmLocationManager.getGpsStatue(null)でエラーになる？？？
                if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    Activity#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for Activity#requestPermissions for more details.
                    return;
                }

                boolean updated = false;

                if (event == GpsStatus.GPS_EVENT_SATELLITE_STATUS) {
                    Iterable<GpsSatellite> satellites = mLocationManager.getGpsStatus(null).getSatellites();
                    int sat_used = 0;
                    int sat_total = 0; // GpsStaus.getMaxSatellites() の返り値が謎
                    for (GpsSatellite sat : satellites) {
                        if (sat.usedInFix()) {
                            sat_used++;
                        }
                        sat_total++;
                    }
                    if (numGps != sat_total || numUseGps != sat_used) {
                        numGps = sat_total;
                        numUseGps = sat_used;
                        updated = true;
                    }
                } else {
                    if (lastGpsStatus != event){
                        lastGpsStatus = event;
                    }
                }
                if (updated){
                    updateStatus();
                }
            }
        };
        checkLocationPermission();
        mLocationManager.addGpsStatusListener(mGpsStatusListener);  // ここから権限が必要らしい
    }
    Criteria getCriteria(){
        Criteria criteria = new Criteria();
        criteria.setAccuracy(Criteria.ACCURACY_FINE);
        criteria.setPowerRequirement(Criteria.POWER_MEDIUM);
        criteria.setSpeedRequired(false);
        criteria.setAltitudeRequired(false);
        criteria.setBearingRequired(false);
        criteria.setCostAllowed(false);
        criteria.setHorizontalAccuracy(Criteria.ACCURACY_HIGH);
        criteria.setVerticalAccuracy(Criteria.ACCURACY_HIGH);
        return criteria;
    }

    private void checkLocationPermission() {
        Utility.requestLocationPermission(this);
    }

    private void locationStart() {
        mLocationManager.requestLocationUpdates(bestProvider, locationInterval*1000, config.LocationMinDistance, this);
    }

    private void locationStop() {
        mLocationManager.removeUpdates(this);
    }

    @Override
    public void onLocationChanged(Location location) {
        //Log.d("DEBUG", "called onLocationChanged");
        //Log.d("DEBUG", "lat : " + location.getLatitude());
        //Log.d("DEBUG", "lon : " + location.getLongitude());

        locInfo.onLocationChanged(location);
        updateGpsLogger(location);
        updateLocation();
    }

    void updateLocation(){
        locationText.setText( locInfo.toStringLocation() );

        //TODO: 毎回計算せず、必要なitemだけ再計算したいのだが。。
        // 遠いitemは更新間隔を空けるとか、上位５０項目だけ毎回更新するとか。。
        for (int i = 0; i < markListAdapter.getCount(); i++) {
            MarkItem item = markListAdapter.getItem(i);
            item.distance = locInfo.calcDistance(item.latitude, item.longitude);
            item.direction = locInfo.calcDirection(item.latitude, item.longitude);
        }

        if (markListAdapter.getCount()>0) {
            markListAdapter.sortByDistance();
            markListAdapter.notifyDataSetChanged();
        }
    }
    void updateStatus(){
        String text = String.format("%,dm ", (int)locInfo.lastAlt);;
        if (numGps > 0){
            text += "" + numUseGps+"/"+numGps + " ";
        }
        switch (lastGpsStatus){
            case GpsStatus.GPS_EVENT_FIRST_FIX:
                text += "Fix";
                break;
            case GpsStatus.GPS_EVENT_STARTED:
                text += "Started";
                break;
            case GpsStatus.GPS_EVENT_STOPPED:
                text += "Stopped";
                break;
        }
        if (Utility.isNotEmpty(bestProvider)){
            text += " ["+bestProvider+"]";
        }
        statusText.setText(text);

        text = "";
        if (gpsLoggerEnabled){
            text += " <logging>";
        }
        loggingText.setText(text);
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        Log.d("DEBUG", "called onStatusChanged");
        switch (status) {
            case LocationProvider.AVAILABLE:
                Log.d("DEBUG", "AVAILABLE");
                break;
            case LocationProvider.OUT_OF_SERVICE:
                Log.d("DEBUG", "OUT_OF_SERVICE");
                break;
            case LocationProvider.TEMPORARILY_UNAVAILABLE:
                Log.d("DEBUG", "TEMPORARILY_UNAVAILABLE");
                break;
            default:
                Log.d("DEBUG", "DEFAULT");
                break;
        }
    }

    @Override
    public void onProviderDisabled(String provider) {
        Log.d("DEBUG", "called onProviderDisabled");
    }

    @Override
    public void onProviderEnabled(String provider) {
        Log.d("DEBUG", "called onProviderEnabled");
    }

    // --------------------------------------------------------------------------------------------
    // GPS logger
    private boolean gpsLoggerEnabled = false;
    private GPXWriter gpxWriter = null;
    void initGpsLogger()
    {
        gpsLoggerEnabled = pref.getBoolean(pfs.GPSLOGGERENABLED, gpsLoggerEnabled);
        if (gpsLoggerEnabled){
            String filename = pref.getString(pfs.GPSLOGFILENAME, "");
            long starttime = pref.getLong(pfs.GPSLOGSTARTTIME, 0);
            startGpsLogger(filename, starttime);
        }
    }
    void toggleGpsLogger()
    {
        if (!Utility.requestStorageWritePermission(this)) {
            return;
        }

        if (gpsLoggerEnabled){
            stopGpsLogger();
        } else {
            startGpsLoggerNew();
        }

        updateStatus();
    }
    // filenameを指定すると、そのファイルへ追加
    // ただし、追加・継続の場合はGPXWriterでclose()していない（XML footerを書いていない）こと！
    String pendingFilename;
    long pendingStartTime;
    void startGpsLoggerNew()
    {
        startGpsLogger(null, 0);
    }
    void startGpsLogger(String filename, long starttime)
    {
        if (!Utility.requestStorageWritePermission(this)) {
            return;
        }

        if (use_service){
            if (!isGpsLoggerServiceStarted()){
                Log.e("KML", "startGpsLogger: GPS Logger service is not started.");
                pendingFilename = filename;
                pendingStartTime = starttime;
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        startGpsLogger(pendingFilename, pendingStartTime);
                    }
                }, 10);
                return;
            }

            // 適当な場所が見つからないのでここでGPS startさせる
            Criteria criteria = getCriteria();
            gpsLoggerService.startGps(criteria);

            if (Utility.isEmpty(filename)){
                filename = getGpsLoggerFileName();
            }

            if (starttime == 0){
                starttime = System.currentTimeMillis();
                SharedPreferences.Editor edit = pref.edit();
                edit.putLong(pfs.GPSLOGSTARTTIME, starttime);
                edit.apply();
            }

            if (!gpsLoggerService.startLog(filename, starttime)){
                Toast.makeText(this, "Failed to open GPS log file: "+filename, Toast.LENGTH_LONG).show();
                return;
            }
        } else {
            if (gpxWriter != null) {
                return;
            }
            gpxWriter = new GPXWriter();

            if (Utility.isEmpty(filename)){
                filename = getGpsLoggerFileName();
            }
            File file = new File(filename);
            if (!gpxWriter.open(file)){
                Toast.makeText(this, "Failed to open GPS log file: "+filename, Toast.LENGTH_LONG).show();
                return;
            }
            gpxWriter.flush();
        }

        changeGpsLogStatus(true, filename);
        Toast.makeText(this, "Started GPS log: "+filename, Toast.LENGTH_LONG).show();
        Log.i("KML", "GPS log started: "+filename);
    }
    void stopGpsLogger()
    {
        if (use_service){
            if (!isGpsLoggerServiceStarted()){
                Log.e("KML", "stopGpsLogger: GPS Logger service is not started.");
                return;
            }
            gpsLoggerService.stopLog();
            gpsLoggerService.stopGps();
        } else {
            if (gpxWriter == null){
                return;
            }
            gpxWriter.close();
            gpxWriter = null;
        }

        changeGpsLogStatus(false, null);
    }

    void changeGpsLogStatus(boolean enabled, String filename)
    {
        gpsLoggerEnabled = enabled;

        // アプリが強制終了しても継続できるようにするため状態を保存
        SharedPreferences.Editor edit = pref.edit();
        edit.putBoolean(pfs.GPSLOGGERENABLED, gpsLoggerEnabled);
        if (Utility.isNotEmpty(filename)){
            edit.putString(pfs.GPSLOGFILENAME, filename);
        }
        edit.apply();
    }

    static public String getGpsLoggerFileName()
    {
        String path = Utility.getPublicDirectory();
        String datestr = Utility.getDateTimeStr(null);
        return path + "/" + datestr + ".gpx";
    }
    // "yyyymmdd-\d+.gpx"に一致するファイルを列挙
    List<String> findTodayGpsLoggerFileNames(boolean wo_logging)
    {
        String path = Utility.getPublicDirectory();
        String datestr = Utility.getDateStr(null);
        List<String> files = Utility.findMatchFiles(path, datestr + "-\\d+\\.gpx");

        if (wo_logging && gpsLoggerEnabled) {
            // 現在log中のファイルは除く
            String logging_file = pref.getString(pfs.GPSLOGFILENAME, "");
            int index = 0;
            for (String file : files) {
                if (file.equals(logging_file)) {
                    files.remove(index);
                    break;
                }
                index++;
            }
        }

        return files;
    }
    void updateGpsLogger(Location location)
    {
        if (gpxWriter == null) return;
        gpxWriter.append(location);
//        gpxWriter.flush();    // 例外が発生しなければこれがなくても問題は無いが。。
    }
    void flushGpsLogger()
    {
        if (gpxWriter != null)
            gpxWriter.flush();
    }

    // --------------------------------------------------------------------------------------------
    // Mark Button Handlers
    void btnMarkAClick(){
        markCommon("MarkA", "");
    }
    void btnMarkBClick(){
        markCommon("MarkB", "");
    }
    void btnMarkCClick(){
        markCommon("MarkC", "");
    }
    void btnMarkDClick(){
        MarkEditDialog dialog = MarkEditDialog.newInstance(this, null);
        tmpMarkIndex = -1;
        tmpLongitude = locInfo.lastLong;
        tmpLatitude = locInfo.lastLat;
        tmpAltitude = (int)locInfo.lastAlt;
        tmpDate = new Date();
        dialog.show(getSupportFragmentManager(), "mark_edit_dialog");
    }
    @Override
    public void onMarkEditOnClick(String name, String comment, int color, @Nullable MarkItem item) {
        if (tmpMarkIndex == -1){
            // 新規登録
            markCommon(name, comment, tmpLongitude, tmpLatitude, tmpAltitude, tmpDate, color);
            Toast.makeText(this, getString(R.string.msg_marked) + name + " " + comment, Toast.LENGTH_LONG).show();
        } else {
            //TODO: 既存のitemの変更
        }
    }

    @Override
    public void onMarkEditCancelClick() {
        // none?
    }


    // dialog用temporary
    int tmpMarkIndex;
    double tmpLongitude;
    double tmpLatitude;
    int tmpAltitude;
    Date tmpDate;
    //

    MarkItem pendingMarkItem = null;
    int pendingCounter = 0;
    Handler timerHandler;
    void markCommon(String name, String comment)
    {
        Date date = new Date();

        if (!Utility.requestStorageWritePermission(this)) {
            return;
        }

        name = MarkItem.mapName(this, name);

        // 連打対応
        if (pendingMarkItem != null) {
            if (!pendingMarkItem.name.equals(name)){
                // 異なるmarkなのでpendingしているものはすぐに登録
                markCommon(pendingMarkItem);
                pendingMarkItem = null;
            }
        }
        if (pendingMarkItem == null){
            pendingMarkItem = new MarkItem(name, comment, locInfo.lastLong, locInfo.lastLat, (int)locInfo.lastAlt);
            pendingMarkItem.setLocalDate(date);
            pendingCounter = 1;
        } else {
            pendingCounter++;
            pendingMarkItem.comment = Integer.toString(pendingCounter) + getString(R.string.msg_person);
        }

        if (timerHandler == null)
            timerHandler = new Handler();
        timerHandler.removeCallbacksAndMessages(null);
        timerHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                flushPendingMarkItem();
            }
        }, config.MaxStrokeInterval);

        Toast.makeText(this, getString(R.string.msg_marked) + name + " " + Integer.toString(pendingCounter) + " " + getString(R.string.msg_person), Toast.LENGTH_LONG).show();
    }
    void flushPendingMarkItem()
    {
        if (timerHandler != null){
            timerHandler.removeCallbacksAndMessages(null);
        }
        if (pendingMarkItem != null){
            markCommon(pendingMarkItem);
            pendingMarkItem = null;
        }
//        Toast.makeText(this, "Timer expired", Toast.LENGTH_SHORT).show();
    }
    void markCommon(String name, String comment, double longitude, double latitude, int altitude, Date date, int color)
    {
        if (!Utility.requestStorageWritePermission(this)) {
            return;
        }
        MarkItem item = new MarkItem(name, comment, longitude, latitude, altitude);
        item.setLocalDate(date);
        item.color = color;
        markCommon(item);
    }
    void markCommon(MarkItem item)
    {
        String filename = getMarkFileName();
        KMLEditor.add(this, filename, item);
    }
    static public String getMarkFileName()
    {
        String path = Utility.getPublicDirectory();
        String datestr = Utility.getDateStr(null);
        return path + "/" + datestr + "-marks.kml";
    }

    void showMarkHistory()
    {
        flushPendingMarkItem();
        String filename = getMarkFileName();
        MarkHistoryDialog dialog = MarkHistoryDialog.newInstance(this, filename);
        if (dialog != null)
            dialog.show(getSupportFragmentManager(), "mark_history");
    }

    // -------------------------------------------------------------------------------------------
    // Upload GPS/Mark Files
    void actionUpload()
    {
        if (gpsLoggerEnabled){
            Utility.confirmDialog(this, getString(R.string.msg_upload_gps_enabled), new Utility.DialogClickListener() {
                @Override
                public void onYesClick() {
                    actionUpload2();
                }
            });
        } else {
            actionUpload2();
        }
    }
    void actionUpload2()
    {
        int count = 0;
        List<String> files = findTodayGpsLoggerFileNames(true);
        count += files.size();
        String markfile = getMarkFileName();
        if (Utility.fileExists(markfile)) count++;

        if (count == 0){
            Utility.showMessageDialog(this, getString(R.string.msg_no_upload_file), null);
            return;
        }

        int size = 0;
        size += Utility.totalFileSize(files);
        size += new File(markfile).length();

        String msg = String.format(getString(R.string.msg_upload_confirm), count, (int)(size/1000000));
        confirmUploadDialog(msg, new Utility.DialogClickListener() {
            @Override
            public void onYesClick() {
                uploadFiles();
            }
        });
    }
    void confirmUploadDialog(String msg, Utility.DialogClickListener listener)
    {
        Utility.confirmUploadDialog(this, getLayoutInflater(), msg, listener);
    }
    MultiFileUploader fileUploader;
    int totalFileCount;
    void uploadFiles()
    {
        flushPendingMarkItem();
        flushGpsLogger();

        String datestr = Utility.getDateStr(null);
        List<String> files = findTodayGpsLoggerFileNames(true);
        String markfile = getMarkFileName();

        // Build CGI parameters
        Map<String, String> params = new HashMap<>();
        params.put("date", datestr);
        params.put("album", "1");
        params.put("url", Utility.getCurrentUploadUrl());

        createFileUploader(params);

        totalFileCount = files.size();
        if (Utility.fileExists(markfile)){
            totalFileCount++;
            fileUploader.startUpload(markfile);
        }

        for (int i=0;i<files.size();i++){
            String file = files.get(i);
            Log.i("upload", "start upload - "+file);
            fileUploader.startUpload(file);
        }
    }
    void createFileUploader(Map<String, String> params)
    {
        String url = params.get("url");
        fileUploader = new MultiFileUploader(url, params, new FileUploader.UploadProgressListener() {
            @Override
            public void onProgressUpdate(int queueing, int uploaded, String uploadedImagePath) {
                if (Utility.isNotEmpty(uploadedImagePath)){
                    File file = new File(uploadedImagePath);
                    // 頻度が高いと出ない？
                    if (uploaded % 5 == 1) {
                        Toast.makeText(MainActivity.this, "Uploaded: " + file.getName() + " Remaining: " + Integer.toString(totalFileCount - uploaded), Toast.LENGTH_SHORT).show();
                    }
                    Log.i("onProg", "uploaded - "+uploadedImagePath);
                }
            }

            @Override
            public void onCompleted() {
                fileUploader = null;
                String msg = getString(R.string.msg_upload_completed);
                Toast.makeText(MainActivity.this, msg, Toast.LENGTH_LONG).show();
                Utility.showMessageDialog(MainActivity.this, msg, null);
            }

            @Override
            public void onFailed(int responseCode) {
                if (!fileUploader.isStop()) {
                    fileUploader.forceToStop();
                    Toast.makeText(MainActivity.this, String.format(getString(R.string.msg_upload_failed), responseCode, totalFileCount), Toast.LENGTH_LONG).show();
                }
                fileUploader = null;
                String msg = String.format("Uploadに失敗しました %d 残り：%d", responseCode, totalFileCount);
                Utility.showMessageDialog(MainActivity.this, msg, null);
            }
        });
    }

    void showLoggingStatus(){
        String msg = "時間=xx:xx:xx 点：xxx points";
        if (isGpsLoggerServiceStarted()){
            int pastime = gpsLoggerService.getPastTime();
            msg = Utility.secondsToHMS(pastime);
            msg += "\n";
            int points = gpsLoggerService.getNumPoints();
            msg += Integer.toString(points) + "[points]";
        }
        Utility.showMessageDialog(this, msg, null);
    }

    // -------------------------------------------------------------------------------------------
    void startGoogleMapHere(){
        if (!locInfo.isValid())
            return;

        Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse("geo:0,0?q="+locInfo.lastLat+","+locInfo.lastLong+"(Now)"));
        startActivity(i);
    }

    void startGoogleMap(int position){
        MarkItem item = markListAdapter.getItem(position);
        Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse("geo:0,0?q="+item.latitude+","+item.longitude+"("+item.name+")"));
        startActivity(i);
    }

    void startGoogleMapNavi(int position){
        MarkItem item = markListAdapter.getItem(position);

        Intent i = new Intent();
        i.setAction(Intent.ACTION_VIEW);
        i.setClassName("com.google.android.apps.maps", "com.google.android.maps.driveabout.app.NavigationActivity");

        Uri uri = Uri.parse("google.navigation:///?ll="+item.latitude+","+item.longitude+"&q="+item.name);
        i.setData(uri);
        startActivity(i);
    }
    // -------------------------------------------------------------------------------------------
    // debug
    boolean existsGpsEmulatorFile(){
        return Utility.fileExists(config.DEBUG_GPS_EMULATION_FILE);
    }
    GPSEmulator gpsEmulator;
    void emulateGps(){
        if (gpsEmulator != null){
            gpsEmulator.stopEmulation();
        } else {
            gpsEmulator = new GPSEmulator();
            gpsEmulator.setGpsEmulatorListener(this);
            gpsEmulator.startEmulation(config.DEBUG_GPS_EMULATION_FILE);
        }
    }
    @Override
    public void onEmulationFinished() {
        gpsEmulator = null;
    }
}
