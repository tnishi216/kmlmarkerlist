package com.reliefoffice.kmlmarkerlist;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class KMLWriter {
    private FileWriter kmlWriter;
    public KMLWriter()
    {
    }
    public boolean open(File file)
    {
        try {
            kmlWriter = new FileWriter(file);
            kmlWriter.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
            kmlWriter.write("<kml xmlns=\"http://www.opengis.net/kml/2.2\">\n");
            kmlWriter.write("<Document>\n");
            kmlWriter.write("<name>Track</name>\n");
            writeMarkerStyles();
        } catch (IOException e) {
            e.printStackTrace();
            kmlWriter = null;
            return false;
        }
        return true;
    }
    public boolean close()
    {
        if (kmlWriter == null)
            return false;

        try {
            kmlWriter.write("</Document>\n");
            kmlWriter.write("</kml>\n");
            kmlWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
            kmlWriter = null;
            return false;
        }
        kmlWriter = null;
        return true;
    }
    void writeMarkerStyles(){
        for (int i = 0; i < styleIds.length; i++) {
            String styleId = getColorStyleId(i);
            writeMarkerStyle(styleId, i);
        }
    }
    boolean writeMarkerStyle(String styleId, int colorIndex){
        String colorString = getColorIndexString(colorIndex);
        try {
            kmlWriter.write("  <Style id=\""+ styleId +"\">\n");
            kmlWriter.write("    <IconStyle>\n");
            kmlWriter.write("      <color>"+ colorString +"</color>\n");
            kmlWriter.write("    </IconStyle>\n");
            kmlWriter.write("  </Style>\n");
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }
    public boolean append(String name, String comment, double longitude, double latitude, int altitude, String timestamp, int colorIndex)
    {
        if (kmlWriter == null)
            return false;

        // KMLファイルに位置情報を追記
        try {
            kmlWriter.write("        <Placemark>\n");
            if (Utility.isNotEmpty(name)) kmlWriter.write("            <name>" + escapeXml(name) + "</name>\n");
            if (Utility.isNotEmpty(comment)) kmlWriter.write("            <description>" + escapeXml(comment) + "</description>\n");
            String styleId = getColorStyleId(colorIndex);
            if (Utility.isNotEmpty(styleId)) kmlWriter.write("            <styleUrl>#" + styleId + "</styleUrl>\n");
            kmlWriter.write("            <Point>\n");
            kmlWriter.write("                <coordinates>" + Double.toString(longitude) + "," + Double.toString(latitude) + "," + Integer.toString(altitude) + "</coordinates>\n");
            kmlWriter.write("            </Point>\n");
            if (Utility.isNotEmpty(timestamp)){
                kmlWriter.write("            <TimeStamp><when>" + timestamp + "</when></TimeStamp>\n");
            }
            kmlWriter.write("        </Placemark>\n");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return true;
    }
    public boolean write(List<MarkItem> items)
    {
        for (MarkItem item: items){
            int colorIndex = findColorIndex(item.color);
            if (!append(item.name, item.comment, item.longitude, item.latitude, item.altitude, item.getDateString(), colorIndex)){
                return false;
            }
        }
        return true;
    }
    String[] styleIds = {
        "icon-red",
        "icon-grn",
        "icon-blu",
        "icon-ylw",
        "icon-mag",
        "icon-org",
        "icon-cya",
        "icon-pnk",
        "icon-blk",
    };
    int[] colors = {
        config.COLOR_RED,
        config.COLOR_GREEN,
        config.COLOR_BLUE,
        config.COLOR_YELLOW,
        config.COLOR_MAGENTA,
        config.COLOR_ORANGE,
        config.COLOR_CYAN,
        config.COLOR_PINK,
        config.COLOR_BLACK,
    };
    String getColorString(int color){
        return String.format("%02X%02X%02X", (color >> 16) & 0xff, (color >> 8) & 0xff, color & 0xff);
    }
    String getColorIndexString(int colorIndex){
        return getColorString( colors[colorIndex] );
    }
    // color index -> style ID
    String getColorStyleId(int colorIndex){
        if (colorIndex < 0) return null;
        int color = colors[colorIndex];
        return styleIds[colorIndex % styleIds.length] + "-" + getColorString( color );
    }
    // color value -> color index
    int findColorIndex(int color){
        if (color == config.COLOR_INVALID)
            return -1;
        for (int i = 0; i < colors.length; i++) {
            if (colors[i] == color){
                return i;
            }
        }
        return -1;
    }
    private static String escapeXml(String input) {
        return Utility.escapeXml(input);
    }
}
