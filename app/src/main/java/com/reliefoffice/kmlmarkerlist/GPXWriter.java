package com.reliefoffice.kmlmarkerlist;

import android.location.Location;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class GPXWriter {
    private FileWriter gpxWriter;
    private boolean openedAsAppend;
    public GPXWriter()
    {
    }
    public boolean isOpenedAsAppend(){
        return openedAsAppend;
    }
    public boolean open(File file)
    {
        boolean exists = Utility.fileExists(file.getAbsolutePath());
        openedAsAppend = exists;
        try {
            gpxWriter = new FileWriter(file, true);
            if (!exists) {  // 新規作成の場合
                gpxWriter.write("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\" ?>\n");
                gpxWriter.write("<gpx xmlns=\"http://www.topografix.com/GPX/1/1\" version=\"1.1\" creator=\"YourAppName\">\n");
                gpxWriter.write("<trk>\n");
                gpxWriter.write("<name>Track</name>\n");
                gpxWriter.write("<trkseg>\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
            gpxWriter = null;
            return false;
        }
        return true;
    }
    public boolean append(Location location)
    {
        if (gpxWriter == null)
            return false;

        // GPXファイルに位置情報を追記
        try {
            gpxWriter.write("  <trkpt lat=\"" + location.getLatitude() + "\" lon=\"" + location.getLongitude() + "\">\n");
            gpxWriter.write("    <ele>" + location.getAltitude() + "</ele>\n");
            gpxWriter.write("    <time>" + new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.getDefault()).format(new Date()) + "</time>\n");
            gpxWriter.write("  </trkpt>\n");
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }
    public boolean flush()
    {
        if (gpxWriter == null)
            return false;
        try {
            gpxWriter.flush();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return true;
    }
    public boolean close()
    {
        if (gpxWriter == null)
            return false;

        // GPXファイルの終了タグを追記して保存
        try {
            gpxWriter.write("</trkseg>\n");
            gpxWriter.write("</trk>\n");
            gpxWriter.write("</gpx>\n");
            gpxWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
            gpxWriter = null;
            return false;
        }
        gpxWriter = null;
        return true;
    }
}
