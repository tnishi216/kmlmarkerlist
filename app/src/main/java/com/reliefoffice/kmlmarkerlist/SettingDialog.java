package com.reliefoffice.kmlmarkerlist;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import androidx.fragment.app.DialogFragment;

import com.reliefoffice.kmlmarkerlist.R;

public class SettingDialog extends DialogFragment {
    public static SettingDialog newInstance(Context context) {
        SettingDialog fragment = new SettingDialog(context);
        Bundle args = new Bundle();
//        args.putString(ARG_PARAM1, markfilename);
//        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    Context context;
    SharedPreferences pref;
    EditText editDefMarkName;

    SettingDialog(Context context)
    {
        this.context = context;
        pref = context.getSharedPreferences(config.PrefName, Context.MODE_PRIVATE);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
//            mParam1 = getArguments().getString(ARG_PARAM1);
//            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater inflater = requireActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.fragment_setting, null);

        editDefMarkName = view.findViewById(R.id.editDefMarkName);

        String text = pref.getString(pfs.DEFAULTDEFMARKNAME, "");
        editDefMarkName.setText(text);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(view)
                .setPositiveButton(getString(R.string.label_close), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String text = editDefMarkName.getText().toString();
                        SharedPreferences.Editor edit = pref.edit();
                        edit.putString(pfs.DEFAULTDEFMARKNAME, text);
                        edit.apply();
//                        listener.onMarkEditOnClick(name, comment);
                    }
                });
//                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
////                        listener.onMarkEditCancelClick();
//                    }
//                });

        return builder.create();
    }

    @Override
    public void onResume() {
        super.onResume();
        Utility.requestFocus(editDefMarkName, getContext());
    }
}
