package com.reliefoffice.kmlmarkerlist;

import android.content.Context;

import java.io.File;
import java.util.List;

public class KMLEditor {
    Context context;
    String filename;
    public KMLEditor(Context context, String filename)
    {
        this.context = context;
        this.filename = filename;
    }
    public static boolean add(Context context, String filename, MarkItem item)
    {
        KMLEditor editor = new KMLEditor(context, filename);
        return editor.add(item);
    }
    public boolean add(MarkItem item)
    {
        KMLReader reader = new KMLReader(context);
        List<MarkItem> items = reader.load(filename);
        items.add(item);
        KMLWriter writer = new KMLWriter();
        if (!writer.open(new File(filename)))
            return false;
        boolean r = writer.write(items);
        writer.close();
        return r;
    }
    public static boolean remove(Context context, String filename, int index)
    {
        KMLEditor editor = new KMLEditor(context,filename);
        return editor.remove(index);
    }
    public boolean remove(int index)
    {
        KMLReader reader = new KMLReader(context);
        List<MarkItem> items = reader.load(filename);
        if (index >= items.size())
            return false;
        items.remove(index);
        KMLWriter writer = new KMLWriter();
        if (!writer.open(new File(filename)))
            return false;
        boolean r = writer.write(items);
        writer.close();
        return r;
    }
    public static boolean edit(Context context, String filename, int index, MarkItem item)
    {
        KMLEditor editor = new KMLEditor(context,filename);
        return editor.edit(index, item);
    }
    public boolean edit(int index, MarkItem item)
    {
        KMLReader reader = new KMLReader(context);
        List<MarkItem> items = reader.load(filename);
        if (index >= items.size())
            return false;
        items.set(index, item);
        KMLWriter writer = new KMLWriter();
        if (!writer.open(new File(filename)))
            return false;
        boolean r = writer.write(items);
        writer.close();
        return r;
    }
}
