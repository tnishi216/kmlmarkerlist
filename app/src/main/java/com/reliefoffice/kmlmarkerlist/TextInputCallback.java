package com.reliefoffice.kmlmarkerlist;

/**
 * Created by tnishi on 2018/03/18.
 */

public interface TextInputCallback {
    void onTextInputClickOk();
    void onTextInputClickCancel();
}
