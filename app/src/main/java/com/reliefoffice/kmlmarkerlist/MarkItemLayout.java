package com.reliefoffice.kmlmarkerlist;

import android.content.Context;
import android.text.Html;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.reliefoffice.kmlmarkerlist.R;

public class MarkItemLayout extends LinearLayout {
    TextView textView;

    public MarkItemLayout(Context context, AttributeSet attrs){
        super(context, attrs);
    }

    @Override
    protected void onFinishInflate(){
        super.onFinishInflate();
        textView  = (TextView)findViewById(R.id.mark_list_item_text);
    }

    public void bindView(MarkItem item, LocationInfo locInfo){
        String str = Utility.escapeXml(item.name);
        if (Utility.isNotEmpty(item.comment))
            str += " " + Utility.escapeXml(item.comment);

        if (item.distance != 0) {
            str += " [";

            int dist = (int)item.distance;
            str += Utility.distanceStr(dist);
            str += " ";
            int relative_direction = locInfo.moveDirection - item.direction;
            str += Utility.directionLevelStrStart(relative_direction);
            str += Utility.directionStr(item.direction);
            str += Utility.directionLevelStrEnd();

//            if (locInfo.moveDistance > 1)
//            {
//                str += Utility.directionLevelStr(relative_direction);
//            }

            str += "]";
        }

        textView.setText(Html.fromHtml(str));
    }
}
