package com.reliefoffice.kmlmarkerlist;

import android.os.AsyncTask;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayDeque;
import java.util.Date;
import java.util.Map;
import java.util.Queue;

public class FileUploader {

    private String UPLOAD_URL;
    private Map<String, String> params;
    private UploadProgressListener listener;
    private UploadTask uploadTask = null;
    private int responseCode;

    public FileUploader(String url, Map<String, String> params, UploadProgressListener listener)
    {
        UPLOAD_URL = url;
        this.params = params;
        this.listener = listener;
        stopFlag = false;
    }

    public void startUpload(String imagePath)
    {
        if (uploadTask == null){
            uploadTask = new UploadTask(imagePath);
            uploadTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else {
            uploadTask.add(imagePath);
        }
    }

    public int getQueueingCount()
    {
        if (uploadTask != null){
            return uploadTask.getQueueingCount();
        }
        return 0;
    }
    public int getUploadedCount()
    {
        if (uploadTask != null){
            return uploadTask.getUploadedCount();
        }
        return 0;
    }

    // エラーが発生したとき、それ以降のuploadをcancelするため
    private static boolean stopFlag = false;

    public void clearStopFlag()
    {
        stopFlag = false;
    }
    public static void forceToStop()
    {
        stopFlag = true;
    }
    public boolean isStop()
    {
        return stopFlag;
    }

    private class UploadTask extends AsyncTask<Void, Integer, Boolean> {
        private Queue<String> imagePaths = new ArrayDeque<>();
        private int uploadedCount = 0;
        private String lastImagePath;
        public UploadTask(String imagePath)
        {
            imagePaths.add(imagePath);
        }
        public void add(String imagePath)
        {
            imagePaths.add(imagePath);
        }
        public int getQueueingCount()
        {
            return imagePaths.size();
        }
        public int getUploadedCount()
        {
            return uploadedCount;
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            if (stopFlag){
                return false;
            }

            trustAllCertificates();

            Boolean result = false;
            while (imagePaths.size() > 0){
                String imagePath = imagePaths.poll();
                try {
                    result = upload(imagePath);
                    if (!result){
                        break;
                    }
                    uploadedCount++;
                    lastImagePath = imagePath;
                    publishProgress();
                    if (stopFlag){
                        break;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return result;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);
            if (listener!=null){
                if (result){
                    listener.onCompleted();
                } else {
                    listener.onFailed(responseCode);
                }
            }
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            if (listener != null) {
                listener.onProgressUpdate(imagePaths.size(), uploadedCount, lastImagePath); // 進捗率を計算してコールバックする
            }
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            if (listener!=null){
                listener.onFailed(0);
            }
        }

        public void progress(int progress)
        {
            publishProgress(progress);
        }
    }

    private boolean upload(String imagePath) throws IOException {
        File imageFile = new File(imagePath);

        URL url = new URL(UPLOAD_URL);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setDoOutput(true);
        conn.setRequestMethod("POST");

        // マルチパートフォームデータを使用するために必要な設定
        String boundary = "---------------------------" + System.currentTimeMillis();
        conn.setRequestProperty("Content-Type", "multipart/form-data; boundary=" + boundary);

        uploadStream1(conn, imageFile, boundary);

        boolean result = false;
        responseCode = conn.getResponseCode();
        if (responseCode == HttpURLConnection.HTTP_OK) {
            // アップロード成功時の処理
            BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String response = in.readLine();
            in.close();
            result = true;
        } else {
            // アップロード失敗時の処理
            result = false;
        }
        conn.disconnect();
        return result;
    }

    private void uploadStream1(HttpURLConnection conn, File imageFile, String boundary) throws IOException {
        OutputStream outputStream = conn.getOutputStream();

        PrintWriter writer = new PrintWriter(new OutputStreamWriter(outputStream, "UTF-8"), true);

        // ファイルデータをアップロードするためのリクエストパラメータを設定
        writer.append("--" + boundary).append("\r\n");
        writer.append("Content-Disposition: form-data; name=\"file\"; filename=\"" + imageFile.getName() + "\"").append("\r\n");
//        writer.append("Content-Type: " + URLConnection.guessContentTypeFromName(imageFile.getName())).append("\r\n");
        writer.append("Content-Type: application/octet-stream\r\n");
        writer.append("Content-Transfer-Encoding: binary\r\n");
        writer.append("\r\n");
        writer.flush();

        FileInputStream fileInputStream = new FileInputStream(imageFile);
        byte[] buffer = new byte[4096];
        int bytesRead = -1;
        long uploadedBytes = 0; // アップロード済みのバイト数
        long totalBytes = imageFile.length(); // ファイルの合計バイト数
        while ((bytesRead = fileInputStream.read(buffer)) != -1) {
            outputStream.write(buffer, 0, bytesRead);
            uploadedBytes += bytesRead;
            int progress = (int) ((uploadedBytes * 100) / totalBytes);
            uploadTask.progress(progress);
        }
        outputStream.flush();
        fileInputStream.close();

        writer.append("\r\n");
        writer.flush();

        // パラメーターをアップロードするためのリクエストパラメータを設定
        final String action = "upload";
        uploadParam(writer, boundary, "action", action);

        // 更新日時の送出
        Date lastDate = new Date(imageFile.lastModified());
        SimpleDateFormat lastDateFmt = new SimpleDateFormat("yyyyMMddHHmmss");
        String lastDateStr = lastDateFmt.format(lastDate);
//        Log.i("date", "date="+lastDateStr);
        uploadParam(writer, boundary, "lastmod", lastDateStr);

        // writer.flush();

        for (Map.Entry<String, String> entry : params.entrySet()) {
            uploadParam(writer, boundary, entry.getKey(), entry.getValue());
        }

        writer.append("--" + boundary + "--").append("\r\n");
        writer.close();
    }

    private void uploadParam(PrintWriter writer, String boundary, String key, String value)
    {
        writer.append("--" + boundary + "\r\n");
        writer.append("Content-Disposition: form-data; name=\"" + key + "\"\r\n");
        writer.append("Content-Type: text/plain\r\n");
        writer.append("\r\n");
        writer.append(value);
        writer.append("\r\n");
    }

    // 参照先: https://qiita.com/informationsea/items/778d9525c3aaded73577
    // うまくいくときとうまくいかないときがある？？
    private void uploadStream2(HttpURLConnection connection, File file, String boundary) throws IOException {

        String filefield = file.getName();
        String filepath = file.getAbsolutePath();
        final String twoHyphens = "--";
//        final String boundary =  "*****"+ UUID.randomUUID().toString()+"*****";
        final String lineEnd = "\r\n";
        final int maxBufferSize = 1024*1024*3;

        DataOutputStream outputStream;

        connection.setDoInput(true);
        connection.setDoOutput(true);
        connection.setUseCaches(false);

        connection.setRequestMethod("POST");
        connection.setRequestProperty("Connection", "Keep-Alive");
        connection.setRequestProperty("Content-Type", "multipart/form-data; boundary="+boundary);

        outputStream = new DataOutputStream(connection.getOutputStream());
        outputStream.writeBytes(twoHyphens + boundary + lineEnd);
        outputStream.writeBytes("Content-Disposition: form-data; name=\"" + filefield + "\"; filename=\"" + file.getName() +"\"" + lineEnd);
        outputStream.writeBytes("Content-Type: application/octet-stream" + lineEnd);
        outputStream.writeBytes("Content-Transfer-Encoding: binary" + lineEnd);
        outputStream.writeBytes(lineEnd);

        FileInputStream fileInputStream = new FileInputStream(filepath);
        int bytesAvailable = fileInputStream.available();
        int bufferSize = Math.min(bytesAvailable, maxBufferSize);
        byte[] buffer = new byte[bufferSize];

        int bytesRead = fileInputStream.read(buffer, 0, bufferSize);
        while(bytesRead > 0) {
            outputStream.write(buffer, 0, bufferSize);
            bytesAvailable = fileInputStream.available();
            bufferSize = Math.min(bytesAvailable, maxBufferSize);
            bytesRead = fileInputStream.read(buffer, 0, bufferSize);
        }

        outputStream.writeBytes(lineEnd);

//        for (Map.Entry<String, String> entry : textdata.entrySet()) {
//            outputStream.writeBytes(twoHyphens + boundary + lineEnd);
//            outputStream.writeBytes("Content-Disposition: form-data; name=\"" + entry.getKey() + "\"" + lineEnd);
//            outputStream.writeBytes("Content-Type: text/plain"+lineEnd);
//            outputStream.writeBytes(lineEnd);
//            outputStream.writeBytes(entry.getValue());
//            outputStream.writeBytes(lineEnd);
//        }

        outputStream.writeBytes(twoHyphens + boundary + lineEnd);
        outputStream.writeBytes("Content-Disposition: form-data; name=\"" + "action" + "\"" + lineEnd);
        outputStream.writeBytes("Content-Type: text/plain"+lineEnd);
        outputStream.writeBytes(lineEnd);
        outputStream.writeBytes("upload");
        outputStream.writeBytes(lineEnd);

        outputStream.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

        outputStream.close();
    }

    public interface UploadProgressListener {
        void onProgressUpdate(int queing, int uploaded, String uploadedImagePath);
        void onCompleted();
        void onFailed(int responseCode);
    }

    private void trustAllCertificates() {
        Utility.trustAllCertificates();
    }
}
