package com.reliefoffice.kmlmarkerlist;

import android.content.SharedPreferences;
import android.location.Location;

public class LocationInfo {
    public double lastLat = 0;
    public double lastLong = 0;

    public double lastAlt = 0;
    public int lastSpeed = 0;   // km/h
    public double moveDistance = 0;
    public int moveDirection = 0;
    public boolean hasDirection = false;    // GPS locationにdirectionが含まれていたか？

    public boolean isValid()
    {
        return lastLat != 0 || lastLong != 0;
    }

    public void load(SharedPreferences pref)
    {
        lastLat = pref.getFloat(pfs.LASTLAT, (float) lastLat);
        lastLong = pref.getFloat(pfs.LASTLONG, (float) lastLong);
    }
    public void save(SharedPreferences pref)
    {
        SharedPreferences.Editor edit = pref.edit();
        edit.putFloat(pfs.LASTLAT, (float) lastLat);
        edit.putFloat(pfs.LASTLONG, (float) lastLong);
        edit.apply();
    }

    public void onLocationChanged(Location location)
    {
        if (lastLong == location.getLongitude() && lastLat == location.getLatitude() && lastAlt == location.getAltitude())
            return;
        moveDistance = Utility.calcDistance(lastLat, lastLong, location.getLatitude(), location.getLongitude());
        hasDirection = location.hasBearing();
        if (hasDirection){
            // 速度が出る移動のときはhasDirection=trueになる
            moveDirection = (int)location.getBearing();
        } else {
            if (moveDistance != 0)
                moveDirection = Utility.calcDirection(lastLat, lastLong, location.getLatitude(), location.getLongitude());
        }
        lastLong = location.getLongitude();
        lastLat = location.getLatitude();
        lastAlt = location.getAltitude();
        float speed = location.getSpeed();
        if (speed == 0){
            lastSpeed = 0;
        } else {
            lastSpeed = (int) (speed * 3.6);
        }
    }

    public String toStringLocation()
    {
        String text = "";
        if (lastLong!=0 && lastLat!=0) {
//            text += lastLong + " " + lastLat; // 小数点形式
            text += Utility.decimalToDMS(lastLong) + " " + Utility.decimalToDMS(lastLat);   // 度分秒形式
            if (moveDistance!=0){
                text += "\n" + Utility.distanceStr((int)moveDistance) + " " + Utility.directionStr(moveDirection);
                if (lastSpeed != 0){
                    text += " " + Integer.toString(lastSpeed) + "km/h";
                }
            }
        }
        return text;
    }

    public double calcDistance(double lat, double lng)
    {
        return Utility.calcDistance(lastLat, lastLong, lat, lng);
    }

    public int calcDirection(double lat, double lng)
    {
        return Utility.calcDirection(lastLat, lastLong, lat, lng);
    }
}
