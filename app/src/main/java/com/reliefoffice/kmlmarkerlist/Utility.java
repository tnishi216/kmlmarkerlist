package com.reliefoffice.kmlmarkerlist;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

public class Utility {
    public static final boolean isEmpty(String s){
        return s==null || s.isEmpty();
    }
    public static final boolean isNotEmpty(String s){
        return s!=null && !s.isEmpty();
    }
    public static final boolean isEqual(String s1, String s2)
    {
        if (isEmpty(s1)){
            return isEmpty(s2);
        }
        if (isEmpty(s2)) return false;
        return s1.equals(s2);
    }

    // ３桁区切り
    public static final String itocs(int value){
        return NumberFormat.getNumberInstance().format(value);
    }
    public static final String itocs(long value){
        return NumberFormat.getNumberInstance().format(value);
    }

    public static String escapeXml(String input) {
        return input
                .replace("&", "&amp;")
                .replace("<", "&lt;")
                .replace(">", "&gt;")
                .replace("\"", "&quot;")
                .replace("'", "&apos;");
    }

    public static boolean fileExists(String filename){
        File file = new File(filename);
        return file.exists();
    }

    public static String initialFileDirectory(){
        //return "/storage/emulated/0";
        return Environment.getExternalStorageDirectory().getAbsolutePath();
    }

    public static String getPublicDirectory(){
        return Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getAbsolutePath();
    }

    // local to UTC
    public static Date dateToUTC(Date date)
    {
        long localMillis = date.getTime();
        long utcMillis = localMillis - TimeZone.getDefault().getOffset(localMillis);
        return new Date(utcMillis);
    }
    // UTC to local
    public static Date dateToLocal(Date date)
    {
        long utcMillis = date.getTime();
        TimeZone localTimeZone = TimeZone.getDefault();
        long localMillis = utcMillis + localTimeZone.getOffset(utcMillis);
        return new Date(localMillis);
    }
    public static String getDateStr(Calendar cal)
    {
        if (cal == null) cal = Calendar.getInstance();
        TimeZone tz = TimeZone.getDefault();
        cal.setTimeZone(tz);
        Date date = cal.getTime();
        return new SimpleDateFormat("yyyyMMdd").format(date);
    }

    public static String getDateTimeStr(Calendar cal)
    {
        if (cal == null) cal = Calendar.getInstance();
        TimeZone tz = TimeZone.getDefault();
        cal.setTimeZone(tz);
        Date date = cal.getTime();
        return new SimpleDateFormat("yyyyMMdd-HHmmss").format(date);
    }
    public static String secondsToHMS(int seconds) {
        int hours = seconds / 3600;
        int minutes = (seconds % 3600) / 60;
        int remainingSeconds = seconds % 60;
        return String.format("%02d:%02d:%02d", hours, minutes, remainingSeconds);
    }
    public static List<String> findMatchFiles(String path, String pattern)
    {
        List<String> matchingFiles = new ArrayList<>();
        File folder = new File(path);

        // ユーザーが指定したパターンから正規表現パターンを作成
        Pattern regexPattern = Pattern.compile(pattern);

        // フォルダ内のすべてのファイルをリストアップ
        File[] files = folder.listFiles();
        if (files != null) {
            for (File file : files) {
                if (file.isFile()) {
                    // ファイル名が指定したパターンに一致するか確認
                    Matcher matcher = regexPattern.matcher(file.getName());
                    if (matcher.matches()) {
                        matchingFiles.add(file.getAbsolutePath());
                    }
                }
            }
        }

        return matchingFiles;
    }
    public static int totalFileSize(List<String> files)
    {
        int total = 0;
        for (String filename: files){
            total += new File(filename).length();
        }
        return total;
    }

    // 配列の保存・復元
    public static void saveList(String keyname, List<String> list, SharedPreferences pref)
    {
        SharedPreferences.Editor editor = pref.edit();
        saveList(keyname, list, editor);
        editor.apply();
    }
    public static void saveList(String keyname, List<String> list, SharedPreferences.Editor editor)
    {
        StringBuilder stringBuilder = new StringBuilder();
        for (String item : list) {
            stringBuilder.append(item).append("\t");
        }
        editor.putString(keyname, stringBuilder.toString());
    }
    public static List<String> loadList(String keyname, SharedPreferences pref)
    {
        String savedString = pref.getString(keyname, null);
        if (savedString != null) {
            String[] items = savedString.split("\t");  // タブ文字で分割
            List<String> loadedList = new ArrayList<>();
            for (String item : items) {
                loadedList.add(item);
            }
            return loadedList;
        }
        return new ArrayList<>();
    }
    public static void saveStringIntMap(String keyname, Map<String, Integer> data, SharedPreferences pref){
        SharedPreferences.Editor editor = pref.edit();
        saveStringIntMap(keyname, data, editor);
        editor.apply();
    }
    public static void saveStringIntMap(String keyname, Map<String, Integer> data, SharedPreferences.Editor editor)
    {
        JSONObject jsonObject = new JSONObject(data);
        String json = jsonObject.toString();
        editor.putString(keyname, json);
    }
    public static Map<String, Integer> loadStringIntMap(String keyname, SharedPreferences pref)
    {
        String savedJson = pref.getString(keyname, null);
        if (savedJson != null) {
            Map<String, Integer> data = new HashMap<>();
            try {
                JSONObject jsonObject = new JSONObject(savedJson);
                Iterator<String> keys = jsonObject.keys();
                while (keys.hasNext()) {
                    String key = keys.next();
                    int value = jsonObject.getInt(key);
                    data.put(key, value);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return data;
        }
        return new HashMap<>();
    }

    public static final int REQUEST_CODE_PERMISSION = 181;
    public static final boolean permissionGranted(int[] grantResults){
        return grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED;
    }

    public static final boolean requestStorageReadPermission(Activity activity){
        if (ContextCompat.checkSelfPermission(activity, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(activity,
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                    REQUEST_CODE_PERMISSION);
            return false;
        }
        return true;
    }

    public static final boolean requestStorageWritePermission(Activity activity){
        if (Build.VERSION.SDK_INT >= 30){
            if(!Environment.isExternalStorageManager()){
                try {
                    Uri uri = Uri.parse("package:" + BuildConfig.APPLICATION_ID);
                    Intent intent = new Intent(Settings.ACTION_MANAGE_APP_ALL_FILES_ACCESS_PERMISSION, uri);
                    activity.startActivity(intent);
                } catch (Exception ex) {
                    Intent intent = new Intent();
                    intent.setAction(Settings.ACTION_MANAGE_ALL_FILES_ACCESS_PERMISSION);
                    activity.startActivity(intent);
                }
            }
        } else
        {
            if (ContextCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(activity,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        REQUEST_CODE_PERMISSION);
                return false;
            }
        }
        return true;
    }

    public static final boolean requestInternetPermision(Activity activity){
        if (ContextCompat.checkSelfPermission(activity, Manifest.permission.INTERNET) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(activity,
                    new String[]{Manifest.permission.INTERNET},
                    REQUEST_CODE_PERMISSION);
            return false;
        }
        return true;
    }

    public static final boolean requestLocationPermission(Activity activity) {
        if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // パーミッションの許可を取得する
            ActivityCompat.requestPermissions(activity,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, REQUEST_CODE_PERMISSION);
            return false;
        }
        return true;
    }

    public interface DialogClickListener {
        void onYesClick();
    }

    public static final void showMessageDialog(Context context, String msg, DialogClickListener listener)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(msg)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // OKを選択したときの処理
                        if (listener != null){
                            listener.onYesClick();
                        }
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }
    public static void confirmDialog(Context context, String msg, DialogClickListener listener)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(msg)
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // はいを選択したときの処理
                        if (listener != null){
                            listener.onYesClick();
                        }
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // いいえを選択したときの処理
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }
    static int uploadUrlItemIndex = 0;
    static String[] uploadUrlTitles = {
            "自宅",
            "Internet"
    };
    static String[] uploadUrls = {
            config.UPLOAD_URL1,
            config.UPLOAD_URL2
    };
    public static final int getCurrentUploadUrlIndex()
    {
        return uploadUrlItemIndex;
    }
    public static final void setCurrentUploadUrlIndex(int index)
    {
        uploadUrlItemIndex = index;
    }
    public static final String getCurrentUploadUrl()
    {
        return uploadUrls[uploadUrlItemIndex];
    }

    static RadioGroup rgUrl = null;
    public static void confirmUploadDialog(Context context, LayoutInflater inflater, String msg, DialogClickListener listener)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        int[] radiobutton_list = {R.id.radiobutton_url1, R.id.radiobutton_url2};

        builder.setView(inflater.inflate(R.layout.dialog_upload_url, null))
                // Add action buttons
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // sign in the user ...
                        int index = getRadioGroupIndex(rgUrl, radiobutton_list, Utility.getCurrentUploadUrlIndex());
                        setCurrentUploadUrlIndex(index);
                        if (listener != null){
                            listener.onYesClick();
                        }
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.show();

        TextView textView = (TextView) dialog.findViewById(R.id.message);
        textView.setText(msg);

        rgUrl = (RadioGroup) dialog.findViewById(R.id.radiogroup_url);
        setRadioGroupIndex(rgUrl, radiobutton_list, getCurrentUploadUrlIndex());
    }

    public static final double GRS80_A = 6378137.000;//長半径 a(m)
    public static final double GRS80_E2 = 0.00669438002301188;//第一遠心率  eの2乗

    public static double deg2rad(double deg){
        return deg * Math.PI / 180.0;
    }

    public static String decimalToDMS(double decimal) {
        int degrees = (int) decimal;
        double minutesDecimal = (decimal - degrees) * 60;
        int minutes = (int) minutesDecimal;
        double seconds = (minutesDecimal - minutes) * 60;

        DecimalFormat df = new DecimalFormat("00.000");
        String formattedSeconds = df.format(seconds);

        return degrees + "°" + minutes + "'" + formattedSeconds + "\"";
    }

    public static double calcDistance(double lat1, double lng1, double lat2, double lng2){
        double my = deg2rad((lat1 + lat2) / 2.0); //緯度の平均値
        double dy = deg2rad(lat1 - lat2); //緯度の差
        double dx = deg2rad(lng1 - lng2); //経度の差

        //卯酉線曲率半径を求める(東と西を結ぶ線の半径)
        double sinMy = Math.sin(my);
        double w = Math.sqrt(1.0 - GRS80_E2 * sinMy * sinMy);
        double n = GRS80_A / w;

        //子午線曲線半径を求める(北と南を結ぶ線の半径)
        double mnum = GRS80_A * (1 - GRS80_E2);
        double m = mnum / (w * w * w);

        //ヒュベニの公式
        double dym = dy * m;
        double dxncos = dx * n * Math.cos(my);
        return Math.sqrt(dym * dym + dxncos * dxncos);
    }

    public static float calcDistance(Location location, double destLat, double destLng){
        Location destinationLocation = new Location("");
        destinationLocation.setLatitude(destLat);
        destinationLocation.setLongitude(destLng);
        return location.distanceTo(destinationLocation);
    }

    public static int calcDirection(double latitude1, double longitude1, double latitude2, double longitude2) {
        double lat1 = Math.toRadians(latitude1);
        double lat2 = Math.toRadians(latitude2);
        double lng1 = Math.toRadians(longitude1);
        double lng2 = Math.toRadians(longitude2);
        double Y = Math.sin(lng2 - lng1) * Math.cos(lat2);
        double X = Math.cos(lat1) * Math.sin(lat2) - Math.sin(lat1) * Math.cos(lat2) * Math.cos(lng2 - lng1);
        double deg = Math.toDegrees(Math.atan2(Y, X));
        double angle = (deg + 360) % 360;
        return (int) (Math.abs(angle) + (1 / 7200));
    }

    public static float calcDirection(Location location, double destLat, double destLng){
        Location destinationLocation = new Location("");
        destinationLocation.setLatitude(destLat);
        destinationLocation.setLongitude(destLng);
        return location.bearingTo(destinationLocation);  // 方角
    }

    public static int directionLevel(int direction)
    {
        while (direction < 0){
            direction += 360;
        }

        if (direction<45 || direction > 360-45)
            return 0;   // 近づいている
        else if (direction < 90 || direction > 360-90)
            return 1;   // 近づいている（やや横ばい）
        else if (direction < 90+45 || direction > 360-(90+45))
            return 2;   // 遠ざかっている（やや横ばい）
        return 3;   // 遠ざかっている
    }
    public static String directionLevelColorStr(int direction){
        int level = directionLevel(direction);
        String color;
        if (level == 0)
            color = "0000FF";
        else if (level == 1)
            color = "00C000";
        else if (level == 2)
            color = "FF00FF";
        else color = "FF0000";

        return color;
    }
    public static String directionLevelStrStart(int direction) {
        String color = directionLevelColorStr(direction);
        return " <font color=\"#" + color + "\">";
    }
    public static String directionLevelStrEnd(){
        return "</font>";
    }
    public static String directionLevelStr(int direction)
    {
        String s = directionLevelStrStart(direction);
        int level = directionLevel(direction);
        s += Integer.toString(level) + " " + Integer.toString(direction);
        s += directionLevelStrEnd();

        return s;
    }

    // distance : meter unit
    public static String distanceStr(int distance){
        if (distance >= 1000){
            return String.valueOf(distance/1000) + "." + String.valueOf((distance%1000)/100) + "km";
        } else {
            return String.valueOf(distance) + "m";
        }
    }

    public static String directionStr(int direction){
        if (direction<23)
            return "N";
        else if (direction < 45+23)
            return "NE";
        else if (direction < 90+23)
            return "E";
        else if (direction < 135+23)
            return "SE";
        else if (direction < 180+23)
            return "S";
        else if (direction < 225+23)
            return "SW";
        else if (direction < 270+23)
            return "W";
        else if (direction < 315+23)
            return "NW";
        return "N";
    }
    public static String directionStr(int direction, boolean small){
        String str = directionStr(direction);
        if (small){
            return str.toLowerCase();
        }
        return str;
    }

    // GUI Helpers
    public static void requestFocus(View control, Context context)
    {
        control.requestFocus();
        control.post(new Runnable() {
            @Override
            public void run() {
                InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(control, InputMethodManager.SHOW_IMPLICIT);
            }
        });
    }
    public static void hideInputMethod(View control, Context context){
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(control.getWindowToken(), 0);
    }
    public static int getRadioGroupIndex(RadioGroup rg, int[] id_list, int default_index)
    {
        for (int i=0;i<id_list.length;i++){
            if (rg.getCheckedRadioButtonId() == id_list[i]){
                return i;
            }
        }
        return default_index;
    }
    public static void setRadioGroupIndex(RadioGroup rg, int[] id_list, int index)
    {
        for (int i=0;i<id_list.length;i++){
            if (index == i){
                rg.check(id_list[i]);
                break;
            }
        }
        // assert(false)
    }

    public static void trustAllCertificates() {
        try {
            TrustManager[] trustAllCerts = new TrustManager[] {
                    new X509TrustManager() {
                        public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
                        }

                        public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
                        }

                        public X509Certificate[] getAcceptedIssuers() {
                            return new X509Certificate[0];
                        }
                    }
            };

            SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, trustAllCerts, new SecureRandom());

            HttpsURLConnection.setDefaultSSLSocketFactory(sslContext.getSocketFactory());
            HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {
                @Override
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
