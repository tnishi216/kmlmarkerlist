package com.reliefoffice.kmlmarkerlist;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import java.util.List;

public class MarkEditDialog extends DialogFragment {

    private @Nullable MarkItem item;
    private AlertDialog dialog;
    private EditText editName;
    private EditText editComment;
    private ColorPicker colorPicker;
    private Button okButton;
    private SharedPreferences pref;

    TextHistoryList markHistory = new TextHistoryList(config.MAX_MARK_NAME_HISTORY);

    public interface MarkEditDialogListener {
        void onMarkEditOnClick(String name, String comment, int color, @Nullable MarkItem item);
        void onMarkEditCancelClick();
    }

    private MarkEditDialogListener listener;

    //    public TextInputDialog() {
//        // Required empty public constructor
//    }
    MarkEditDialog(MarkEditDialogListener listener, MarkItem item) {
        this.listener = listener;
        this.item = item;
    }

    public static MarkEditDialog newInstance(MarkEditDialogListener listener, @Nullable MarkItem item) {
        MarkEditDialog fragment = new MarkEditDialog(listener, item);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        pref = getContext().getSharedPreferences(config.PrefName, Context.MODE_PRIVATE);

        LayoutInflater inflater = requireActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.fragment_mark_edit_dialog, null);
        editName = view.findViewById(R.id.editName);
        editComment = view.findViewById(R.id.editComment);
        int color = pref.getInt(pfs.LAST_MARK_COLOR, 0);

        if (item != null){
            editName.setText(item.name);
            editComment.setText(item.comment);
            color = item.color;
        }

        if (editName.getText().toString().isEmpty()){
            String text = pref.getString(pfs.DEFAULTDEFMARKNAME, "");
            editName.setText(text);
        }

        Spinner colorSpinner = view.findViewById(R.id.colorSpinner);
        colorPicker = new ColorPicker(requireContext(), colorSpinner);
        colorPicker.setup(color);

        editName.addTextChangedListener(new TextWatcher() {
                                            @Override
                                            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                                            }

                                            @Override
                                            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                                                String name = charSequence.toString();
                                                okButton.setEnabled(Utility.isNotEmpty(name));
                                                if (markHistory.hasText(name)){
                                                    int color = markHistory.getColor(name);
                                                    if (color != config.COLOR_INVALID)
                                                        colorPicker.setColor(color);
                                                }
                                            }

                                            @Override
                                            public void afterTextChanged(Editable editable) {

                                            }
                                        });

        if (markHistory.size() == 0) {
            markHistory.load(pref, pfs.MARK_NAME_HISTORY, pfs.MARK_NAME_HISTORY_COLORS);
        }
        editName.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            showNameHistory(view);
                                        }
                                    });

                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(view)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String name = editName.getText().toString();
                        String comment = editComment.getText().toString();
                        int color = colorPicker.getSelectedColor();

                        if (Utility.isNotEmpty(name)) {
                            if (markHistory.add(name, color)) {
                                markHistory.save(pref, pfs.MARK_NAME_HISTORY, pfs.MARK_NAME_HISTORY_COLORS);
                            }
                        }

                        if (item != null){
                            item.name = name;
                            item.comment = comment;
                            item.color = color;
                        }
                        listener.onMarkEditOnClick(name, comment, color, item);
                        Utility.hideInputMethod(editName, getContext());
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        listener.onMarkEditCancelClick();
                        Utility.hideInputMethod(editName, getContext());
                    }
                });

        dialog = builder.create();
        return dialog;
    }

    @Override
    public void onResume() {
        super.onResume();

        // OK buttonをenable/disableする
        okButton = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
        okButton.setEnabled(!editName.getText().toString().isEmpty());

        if (editName.getText().length() == 0) {
            Utility.requestFocus(editName, getContext());
            showNameHistory(editName);  // 出てくれない。。
        } else {
            Utility.requestFocus(editComment, getContext());
        }
    }


    @Override
    public void onPause() {
        SharedPreferences.Editor editor = pref.edit();
        editor.putInt(pfs.LAST_MARK_COLOR, colorPicker.getSelectedColor());
        editor.apply();

        super.onPause();
    }

    void showNameHistory(View view){
        List<String> textHistoryList = markHistory.getList();
        new TextHistoryMenu(getContext(), view, textHistoryList, new TextHistoryMenu.TextHistoryMenuListener() {
            @Override
            public void onTextSelected(String text) {
                // テキスト履歴を選択
                editName.setText(text);
            }
        });
    }
}

class ColorPicker {
    Context context;
    Spinner colorSpinner;
    int selectedColor;

    public ColorPicker(Context context, Spinner colorSipinner) {
        this.context = context;
        this.colorSpinner = colorSipinner;
    }
    public int getSelectedColor(){
        return selectedColor;
    }
    public void setColor(int color){
        int position = getPositionFromColor(color);
        colorSpinner.setSelection(position);
        selectedColor = getColorFromPosition(position);
    }
    public void setup(int color) {
        // Create an ArrayAdapter with predefined colors
        ArrayAdapter<String> adapter = new ArrayAdapter<>(context,
                android.R.layout.simple_spinner_item,
                new String[]{"Red", "Green", "Blue", "Yellow", "Purple", "Orange", "Cyan", "Pink", "Black"});
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        colorSpinner.setAdapter(adapter);

        // Set spinner item selected listener
        colorSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedColor = getColorFromPosition(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        setColor(color);
    }

    private int getColorFromPosition(int position) {
        switch (position) {
            case 0:
                return config.COLOR_RED;
            case 1:
                return config.COLOR_GREEN;
            case 2:
                return config.COLOR_BLUE;
            case 3:
                return config.COLOR_YELLOW;
            case 4:
                return config.COLOR_MAGENTA;
            case 5:
                return config.COLOR_ORANGE;
            case 6:
                return config.COLOR_CYAN;
            case 7:
                return config.COLOR_PINK;
            default:
                return config.COLOR_BLACK;
        }
    }
    int getPositionFromColor(int color){
        switch (color){
            case config.COLOR_RED: return 0;
            case config.COLOR_GREEN: return 1;
            case config.COLOR_BLUE: return 2;
            case config.COLOR_YELLOW: return 3;
            case config.COLOR_MAGENTA: return 4;
            case config.COLOR_ORANGE: return 5;
            case config.COLOR_CYAN: return 6;
            case config.COLOR_PINK: return 7;
            default:    // BALCK or else
                return 8;
        }
    }
}

