package com.reliefoffice.kmlmarkerlist;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;

import com.reliefoffice.kmlmarkerlist.R;

public class SelectLocationIntervalDialog extends DialogFragment {

    int selected = 0;

    final CharSequence[] items = { "1", "3", "5", "10", "30", "60" };

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.title_location_interval);
        builder.setSingleChoiceItems(items, selected, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                int value = Integer.parseInt((String)items[which]);
                onSelectLocationIntervalSet(value);
                dialog.dismiss();
            }
        });
        builder.setNegativeButton(R.string.label_close, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        return builder.create();
    }
    // value : second
    public void setSelect(int value){
        for (int i=0;i<items.length;i++) {
            if (value <= Integer.parseInt((String) items[i])){
                selected = i;
                break;
            }
        }
    }

    protected void onSelectLocationIntervalSet(int value){}
}
