package com.reliefoffice.kmlmarkerlist;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.reliefoffice.kmlmarkerlist.R;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class MarkListAdapter extends ArrayAdapter<MarkItem> {
    private List<MarkItem> m_listMark;
    public LocationInfo locInfo;

    LayoutInflater layoutInflater;
    int resource;

    MarkListAdapter(Context context, int resource, LocationInfo locInfo) {
        super(context, resource);
        this.locInfo = locInfo;

        layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.resource = resource;
    }

    public void clear(){
        super.clear();
        if (m_listMark != null) {
            m_listMark.clear();
            m_listMark = null;
        }
    }

    public void setList(List<MarkItem> items){
        this.m_listMark = items;
    }

    public void addList(List<MarkItem> items){
        if (m_listMark == null)
            setList(items);
        else
            this.m_listMark.addAll(items);
    }

    @Override
    public int getCount() { return m_listMark != null ? m_listMark.size() : 0 ; }

    @Override
    public MarkItem getItem( int position )
    {
        return m_listMark != null ? m_listMark.get( position ) : null;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        final MarkItemLayout view;
        if (convertView==null){
            view = (MarkItemLayout)layoutInflater.inflate(resource, null);
        } else {
            view = (MarkItemLayout)convertView;
        }

        MarkItem item = getItem(position);
        ImageView imgView = (ImageView)view.findViewById(R.id.mark_list_item_imgview);
        imgView.setBackgroundColor(item.getColor());

        // text sizeの変更
        TextView textView = (TextView)view.findViewById(R.id.mark_list_item_text);
        if (position<5)
            textView.setTextSize( 20 );
        else
            textView.setTextSize( 14 );

        view.bindView(getItem(position), locInfo);
        return view;
    }

    public void sortByName(){
        Collections.sort(m_listMark, new Comparator<MarkItem>() {
            @Override
            public int compare(MarkItem lhs, MarkItem rhs) {
                return lhs.name.compareTo(rhs.name);
            }
        });
    }

    public void sortByDistance(){
        Collections.sort(m_listMark, new Comparator<MarkItem>() {
            @Override
            public int compare(MarkItem lhs, MarkItem rhs) {
                if (lhs.distance == rhs.distance)
                    return 0;
                if (lhs.distance < rhs.distance)
                    return -1;
                else
                    return 1;
            }
        });
    }
}
