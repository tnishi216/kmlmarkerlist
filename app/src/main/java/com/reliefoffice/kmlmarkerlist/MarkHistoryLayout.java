package com.reliefoffice.kmlmarkerlist;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.reliefoffice.kmlmarkerlist.R;

public class MarkHistoryLayout extends LinearLayout {
    TextView textName;
    TextView textComment;
    TextView textTime;

    public MarkHistoryLayout(Context context, AttributeSet attrs){
        super(context, attrs);
    }

    @Override
    protected void onFinishInflate(){
        super.onFinishInflate();
        textName  = (TextView)findViewById(R.id.mark_history_item_name);
        textComment  = (TextView)findViewById(R.id.mark_history_item_comment);
        textTime = (TextView)findViewById(R.id.mark_history_item_time);
    }

    public void bindView(MarkItem item){
        textName.setText(item.name);
        textComment.setText(item.comment);
        if (!item.isDateNull()){
            textTime.setText(item.getDateDispString());
        }
    }
}
