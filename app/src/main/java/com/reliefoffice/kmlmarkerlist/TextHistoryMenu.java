package com.reliefoffice.kmlmarkerlist;

import android.content.Context;
import android.view.View;
import android.widget.PopupMenu;

import java.util.List;

public class TextHistoryMenu {
    public interface TextHistoryMenuListener {
        void onTextSelected(String text);
    }
    TextHistoryMenuListener listener;
    TextHistoryMenu(Context context, View view, List<String> pastTextList, TextHistoryMenuListener listener)
    {
        this.listener = listener;

        PopupMenu popupMenu = new PopupMenu(context, view);
        for (String pastText : pastTextList) {
            popupMenu.getMenu().add(pastText);
        }

        popupMenu.setOnMenuItemClickListener(item -> {
            String selectedText = item.getTitle().toString();
            // 選択されたテキスト
            if (listener != null){
                listener.onTextSelected(selectedText);
            }
            return true;
        });

        popupMenu.show();
    }
}
