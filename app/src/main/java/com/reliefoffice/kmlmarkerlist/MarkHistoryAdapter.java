package com.reliefoffice.kmlmarkerlist;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import androidx.annotation.Nullable;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class MarkHistoryAdapter extends ArrayAdapter<MarkItem> {
    private List<MarkItem> m_listMark;

    LayoutInflater layoutInflater;
    int resource;

    MarkHistoryAdapter(Context context, int resource) {
        super(context, resource);

        layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.resource = resource;
    }

    public void setList(List<MarkItem> items){
        this.m_listMark = items;
    }

    @Override
    public int getCount() { return m_listMark != null ? m_listMark.size() : 0 ; }

    @Override
    public MarkItem getItem( int position )
    {
        return m_listMark != null ? m_listMark.get( position ) : null;
    }

    @Override
    public void insert(@Nullable MarkItem item, int index)
    {
        m_listMark.add(index, item);
        super.insert(item, index);
    }
    @Override
    public void remove(@Nullable MarkItem object) {
        m_listMark.remove(object);
        super.remove(object);
    }

    public void replace(int index, MarkItem item)
    {
        remove(m_listMark.get(index));
        insert(item, index);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        final MarkHistoryLayout view;
        if (convertView==null){
            view = (MarkHistoryLayout)layoutInflater.inflate(resource, null);
        } else {
            view = (MarkHistoryLayout)convertView;
        }

        view.bindView(getItem(position));
        return view;
    }

    public void sortByName(){
        Collections.sort(m_listMark, new Comparator<MarkItem>() {
            @Override
            public int compare(MarkItem lhs, MarkItem rhs) {
                return lhs.name.compareTo(rhs.name);
            }
        });
    }
}
